# README #

## Prerequisites
Need to install `python` on all remote nodes, in order to control the nodes using ansible:
```
#!bash
ansible -e 'ansible_python_interpreter=/usr/bin/python3' -i config/hosts ga4gh-prod -m shell -a "sudo apt-get update && sudo apt-get install python -y"
```
## One-off command
To run a one off command:

```
#!bash
cd ga4gh-cm/ansible
ansible -i config/hosts ga4gh-prod -m shell -a "service elasticsearch status" --become

```
## Complete Setup
Before running the ansible-playbook command below, update `ga4gh-prod.yml` with the correct definition for `es_hosts`, which is a key (hostname) and value (ip adddress of that host) pair, used to automating ES configuration and linux /etc/hosts configuration.

```
#!bash
ansible-playbook -i config/hosts ga4gh-prod.yml
```

## Run playbook locally
To run a playbook locally:

```
#!bash
ansible-playbook -i config/hosts maven.yml --connection=local
```


## Note:
- `config/hosts` is the inventory file
